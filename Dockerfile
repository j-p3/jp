# Use an official Python runtime as a parent image
FROM python:3.9

# Set the working directory to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN apt-get update && apt-get install -y git

# Clone the Git repository containing ops.py into the /app directory
RUN git clone https://gitlab.com/j-p3/jp.git

# Run app.py when the container launches
CMD ["python", "ops.py"]

